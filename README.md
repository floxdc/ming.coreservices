# Core Services of Ming #

![ming-logo.png](https://bitbucket.org/repo/XB5a5y/images/2407079651-ming-logo.png)

Core Services of Ming are a set of F# libraries which can be used to create dictionaries, their containers, and transcriptions for [Ming](https://bitbucket.org/account/user/floxdc/projects/MI).

### Usage ###

`PM> Install-Package Ming.CoreServices -Version 1.1.0`

`PM> Install-Package Ming.ContainerManager -Version 1.1.1`

### Documentation ###

All the documentation is located on the our [wiki](https://bitbucket.org/floxdc/ming.coreservices/wiki/Home) page.

### Community & Support ###

Need help with something that the docs aren't providing an answer to? Visit our [Issues page](https://bitbucket.org/floxdc/ming.coreservices/issues) and join in the conversation.

### Contributing ###

We'd love for you to contribute to the Ming project!

### License & Credits###

This project is licensed under MIT [license](https://bitbucket.org/floxdc/ming.coreservices/src/7187f42f4923b0715aabbafb49b93630ed26be64/LICENSE).

*Ming.Core* uses a modified implementation of an AVL tree written by [Awesome Princess](https://en.wikibooks.org/wiki/F_Sharp_Programming/Advanced_Data_Structures#AVL_Trees).
*Ming.ContainerManager* uses [FsPickler](https://nessos.github.io/FsPickler/) serializer.