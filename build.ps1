$root = (Get-Location).Path

cmd.exe /c 'C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat'
dotnet restore

$path = $root + '\src\Ming.Core'
cd $path
dotnet build

$path = $root + '\src\Ming.DataServices'
cd $path
dotnet build

$path = $root + '\src\Ming.Control'
cd $path
dotnet build

$path = $root + '\src\Tests'
cd $path
dotnet build
dotnet test

Write-Host "Done."