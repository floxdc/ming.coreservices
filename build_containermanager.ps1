$root = (Get-Location).Path

cmd.exe /c 'C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat'
dotnet restore

$path = $root + '\src\Ming.ContainerManager'
cd $path
dotnet build

Write-Host "Done."