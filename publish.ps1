$root = (Get-Location).Path
$output = $root + '\publish\CoreServices'

Get-ChildItem -Path $output -Include * -File -Recurse | foreach { $_.Delete()}

cmd.exe /c 'C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat'

$path = $root + '\src\Ming.Core'
cd $path
dotnet publish --output $output

$path = $root + '\src\Ming.DataServices'
cd $path
dotnet publish --output $output

$path = $root + '\src\Ming.Control'
cd $path
dotnet publish --output $output

Write-Host "Done."