$root = (Get-Location).Path
$output = $root + '\publish\ContainerManager'

Get-ChildItem -Path $output -Include * -File -Recurse | foreach { $_.Delete()}

cmd.exe /c 'C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat'

$path = $root + '\src\Ming.ContainerManager'
cd $path
dotnet publish --output $output

Write-Host "Done."