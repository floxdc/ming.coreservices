﻿namespace Floxdc.Ming.ContainerManager

open System.IO
open Nessos.FsPickler.Json
open Floxdc.Ming.Core.BaseTypes


type ContainerManager() = 
    let serializer = FsPickler.CreateJsonSerializer() 


    member this.Deserialize dictionaryPath = 
        use stream = new System.IO.FileStream(dictionaryPath, FileMode.Open, FileAccess.Read)
        serializer.Deserialize<DictionaryContainer>(stream)
    

    member this.DeserializeFromString dictionaryJson = 
        serializer.UnPickleOfString<DictionaryContainer>(dictionaryJson)


    member this.Serialize dictionaryPath dictionary = 
        use stream = new System.IO.FileStream(dictionaryPath, FileMode.Create, FileAccess.Write)
        serializer.Serialize(stream, dictionary)

    
    member this.SerializeToString dictionary = 
        serializer.PickleToString(dictionary)