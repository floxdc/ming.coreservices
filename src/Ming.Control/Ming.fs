﻿namespace Floxdc.Ming.Control

open Floxdc.Ming.Core.BaseTypes
open Floxdc.Ming.Control.ControlUtils


/// <summary>Transcribes the input string according to supplied dictionary container.</summary>
type Ming(dictionaryContainer: DictionaryContainer, ?markUntranscribed) =
    let container = dictionaryContainer
    let isMarkUntranscribed = defaultArg markUntranscribed false


    let getNormalizationForm (value: string) = 
        match value.ToLower() with 
        | "c" -> System.Text.NormalizationForm.FormC
        | "d" -> System.Text.NormalizationForm.FormD
        | "kc" -> System.Text.NormalizationForm.FormKC
        | "kd" -> System.Text.NormalizationForm.FormKD
        | _ -> raise (System.ArgumentException("Value \"" + value + "\" is not a valid normalization form type name.", "normalizationForm"))

             
    let getTokenizedInput form input = 
        ControlUtils.normalize container form input
        |> ControlUtils.splitString


    let toLower culture (target: string) = 
        target.ToLower(culture)


    let getFormattedOutput output = 
        ControlUtils.combineString isMarkUntranscribed System.String.Empty false output
        |> toLower container.OutputCulture
        |> container.OutputCulture.TextInfo.ToTitleCase


    let getUntranscribedTokensCount target = 
        List.filter (fun (item : TokenizedOutput) -> item.IsUntranscribed) target 
        |> List.length


    let transcribe input form = 
        getTokenizedInput form input
        |> Transcriber.transcribe container.Content

    
    /// <summary>Transcribes the input string with all possible normalization strategies. A return value indicates whether the transcription succeeded.</summary>
    /// <param name="input"> The text that should be transcribed.</param>
    /// <param name="value"> When this method returns, contains the transcribed string value contained in input, if the conversion succeeded, or untranscribed value if the processing failed.</param>
    /// <returns> The method returns a string.</returns>
    member this.TryAuto(input, [<System.Runtime.InteropServices.Out>] value: byref<string>) = 
        let transcribedInC = transcribe input System.Text.NormalizationForm.FormC
        let countC = getUntranscribedTokensCount transcribedInC
            
        let transcribed, isTranscribed =
            match countC with 
            | 0 -> (transcribedInC, true)
            | _ -> 
                let transcribedInD = transcribe input System.Text.NormalizationForm.FormD
                let countD = getUntranscribedTokensCount transcribedInD
                match countC > countD with 
                | true when countD = 0 -> (transcribedInD, true)
                | true -> (transcribedInD, false)
                | false -> (transcribedInC, false)

        value <- getFormattedOutput transcribed
        isTranscribed


    /// <summary>Transcribes the input string according to supplied dictionary container.</summary>
    /// <param name="input"> The text that should be transcribed.</param>
    /// <param name="normalizationForm">Unicode normalization form which is applied to input string. Optional.</param>
    /// <remark> Default: Form C.</remark>
    /// <returns> The method returns a string.</returns>
    member this.Transcribe(input, ?normalizationForm) = 
        defaultArg normalizationForm System.Text.NormalizationForm.FormC
        |> transcribe input
        |> getFormattedOutput


    /// <summary>Transcribes the input string according to supplied dictionary container.</summary>
    /// <param name="input"> The text that should be transcribed.</param>
    /// <param name="normalizationForm"> String representation of unicode normalization form name which is applied to input string.</param>
    /// <returns> The method returns a string.</returns>
    /// <exception cref="System.ArgumentException">Thrown when the specified <paramref name="normalizationForm"/> string value is not a valid normalization form type name.</exception>
    member this.Transcribe(input, normalizationForm) = 
        getNormalizationForm normalizationForm
        |> transcribe input
        |> getFormattedOutput