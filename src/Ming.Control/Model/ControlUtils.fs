﻿module internal Floxdc.Ming.Control.ControlUtils

open Floxdc.Ming.Core
open Floxdc.Ming.Core.BaseTypes


type TokenizedInput = {
        Value: string;
        Delimiter: string
    }


type TokenizedOutput = {
        Value: string;
        Delimiter: string;
        IsUntranscribed: bool
    }


let private untranscribedStarts = "["
let private untranscribedEnds = "]"


let rec private delimitString delimiters (acc: TokenizedInput list) (target: string) = 
    match (target.IndexOfAny(delimiters)) with 
    | -1 -> 
        match target.Length with 
        | 0 -> acc
        | _ -> acc @ [{Value = target; Delimiter = System.String.Empty}]
    | index -> 
        let section = target.Substring(0, index)
        let dilimeter = target.Substring(index, 1)
        let target' = target.Remove(0, index + 1)
        let acc' = acc @ [{Value = section; Delimiter = dilimeter}]

        delimitString delimiters acc' target'


let private getClosingSymbol (tailHead: TokenizedOutput option) = 
    match tailHead with 
    | None -> System.String.Empty
    | Some innerHead -> 
        match innerHead.IsUntranscribed with 
        | true -> System.String.Empty
        | false -> untranscribedEnds + System.String.Empty


let private getOpeningSymbol isPrevTokenUntranscribed = 
    match isPrevTokenUntranscribed with 
    | true -> System.String.Empty
    | false -> untranscribedStarts


let rec combineString isMarkUntranscribed acc isPrevTokenUntranscribed values = 
    match values with 
    | [] -> 
        match isPrevTokenUntranscribed with 
        | true when isMarkUntranscribed -> acc + untranscribedEnds
        | _ -> acc
    | head::tail -> 
        let acc' = 
            //match head.IsUntranscribed with 
            match isMarkUntranscribed with 
            | true -> 
                match head.IsUntranscribed with 
                | true -> 
                    let openingSymbol = getOpeningSymbol isPrevTokenUntranscribed
                    let closingSymbol = getClosingSymbol (List.tryHead tail)
                    acc + openingSymbol + head.Value + closingSymbol + head.Delimiter
                | false -> acc + head.Value + head.Delimiter
            | false -> 
                acc + head.Value + head.Delimiter
        
        combineString isMarkUntranscribed acc' head.IsUntranscribed tail


let normalize (container: DictionaryContainer) form input = 
    let rulesSeq = Floxdc.Ming.Core.Utils.toSeq container.NormalizationRules
        
    StringUtils.normalizeInput container.InputCulture form input
    |> Floxdc.Ming.Core.Utils.applyNormalizationRules rulesSeq


let splitString target = 
    let delimiters = [|' '; '-'|]

    delimitString delimiters [] target