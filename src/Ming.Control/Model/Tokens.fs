﻿module internal Floxdc.Ming.Control.Tokens

open Floxdc.Ming.Core


let rec private setTokens (acc: Map<string, string list>) (keys: string list) = 
    match keys with 
    | [] -> acc
    | keys -> 
        let key = StringUtils.getFirstChar keys.Head
        match acc.ContainsKey(key) with 
        | true -> 
            let value = acc.Item(key) @ [keys.Head]
            let acc' = 
                acc.Remove(key)
                |> Map.add key value
            setTokens acc' keys.Tail
        | false -> 
            let acc' = acc.Add(key, [keys.Head])
            setTokens acc' keys.Tail
 
    
let getTokens acc tree = 
    let keys = Utils.getLeafKeys acc tree
    let empty = new Map<string, string list>(Seq.empty)
        
    setTokens empty keys
    |> Utils.sortMapValuesDesc empty

    
let rec matchToken acc (tokens: Map<string, string list>) input = 
    match (System.String.IsNullOrWhiteSpace input) with 
    | true -> acc
    | false -> 
        let key = StringUtils.getFirstChar input
        match tokens.ContainsKey(key) with 
        | false -> 
            matchToken (acc @ [key]) tokens (input.Substring 1)
        | true -> 
            let topt = 
                List.filter (fun t -> StringUtils.isMatchToken t input) (tokens.Item(key))
                |> List.tryHead
            
            match topt with 
            | None -> acc @ [input]
            | Some token -> 
                let rest = StringUtils.trimToken token input
                matchToken (acc @ [token]) tokens rest