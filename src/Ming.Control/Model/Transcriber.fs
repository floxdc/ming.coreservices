﻿module internal Floxdc.Ming.Control.Transcriber

open Floxdc.Ming.Core.BaseTypes
open Floxdc.Ming.Control
open Floxdc.Ming.Control.ControlUtils


let private matchTranscribed (acc: (string * bool) list) (transcribed: string * bool) = 
    let value, isUntranscribed = transcribed

    match acc with 
    | [] -> 
        [(value, isUntranscribed)]
    | _ ->
        let lastValue, isLastUntranscribed = List.last acc 
        match isUntranscribed = isLastUntranscribed with 
            | true -> 
                let reduced = List.take<string * bool> (acc.Length - 1) acc
                reduced @ [(lastValue + value, isUntranscribed)]
            | false -> 
                acc @ [(value, isUntranscribed)]


let rec private toTokenizedOutput delimiter acc (target: (string * bool) list) = 
    match target with 
    | [] -> acc
    | head::tail -> 
        match tail with 
        | [] -> 
            let result, isUntranscribed = head
            acc @ [{Value = result; Delimiter = delimiter; IsUntranscribed = isUntranscribed}]
        | tail ->
            let result, isUntranscribed = head
            let acc' = acc @ [{Value = result; Delimiter = System.String.Empty; IsUntranscribed = isUntranscribed}]
            toTokenizedOutput delimiter acc' tail


let rec private transcriptToken (tree: AvlTree<Floxdc.Ming.Core.Interfaces.ILeafInfo>) token =
    let leafInfo = new LeafInfo(token, System.String.Empty)
    let value = tree.GetValue leafInfo

    match (System.String.IsNullOrEmpty value) with 
    | true -> 
        match token.Length with 
        | 0 -> (value, false)
        | _ -> (token, true)
    | false -> (value, false)


let private transcript tree (matchedTokens: string list) : (string * bool) list = 
    let tcript = transcriptToken tree

    List.fold (fun acc token -> 
        tcript token
        |> matchTranscribed acc
    ) [] matchedTokens


let transcribe tree tokenizedInput = 
    let matched = 
        Tokens.getTokens [] tree
        |> Tokens.matchToken []
    let tcript = transcript tree
        
    List.map (fun (input: TokenizedInput) ->
        matched input.Value
        |> tcript
        |> toTokenizedOutput input.Delimiter []
    ) tokenizedInput
    |> List.collect id