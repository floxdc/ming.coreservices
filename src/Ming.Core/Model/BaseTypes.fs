﻿namespace Floxdc.Ming.Core.BaseTypes

open System.Globalization
open Floxdc.Ming.Core.Interfaces
open Floxdc.Ming.Core.Tree


type AvlTree<'a when 'a :> ILeafInfo>(tree: 'a Tree) = 
    member this.Contains(value) = 
        contains value tree

    member this.GetValue(key) = 
        getValue key tree

    member this.Height = 
        height tree

    member this.Insert(value) = 
        new AvlTree<'a>(insert value tree)
    
    member this.Left = 
        match tree with 
        | TreeNode(_, l, _, _) -> new AvlTree<'a>(l)
        | Nil -> failwith "No nodes here"
    
    member this.Right = 
        match tree with 
        | TreeNode(_, _, _, r) -> new AvlTree<'a>(r)
        | Nil -> failwith "No nodes here"

    member this.Value = 
        match tree with 
        | TreeNode(_, _, hd, _) -> hd
        | Nil -> failwith "No value here"


        
type LeafInfo(key, value) = 
    interface ILeafInfo with
        member this.Key = key
        member this.Value = value


/// <summary>Initializes a new instance of the DictionaryContainer class to the source tree dictionary, origin-, input-, and output culture names.</summary>
/// <param name="content"> Represents the dictionary as a source tree. This field is read-only. </param>
/// <param name="originCulture"> Represents the source file culture. </param>
/// <param name="inputCulture"> Represents the input text culture. </param>
/// <param name="outputCulture"> Represents the output text culture. </param>
type DictionaryContainer(content: AvlTree<_>, originCulture: string, inputCulture: string, outputCulture: string) = 
    /// <summary> Content </summary>
    /// <value> Represents the dictionary as a source tree. This field is read-only. </value>
    member this.Content = content

    /// <summary> Description </summary>
    /// <value> Represents the additional information about the dictionary. </value>
    [<DefaultValue>]
    val mutable Description : string

    /// <summary> Exceptions </summary>
    /// <value> Represents the list of transcription exceptions. Due to historical reasons, for example. </value>
    [<DefaultValue>]
    [<System.ObsoleteAttribute("Not supported yet.")>]
    val mutable Exceptions : System.Collections.Generic.Dictionary<string, string>
    
    /// <summary> InputCulture </summary>
    /// <value> Represents the input text culture (locale). This field is read-only. </value>
    member this.InputCulture = 
        new CultureInfo(inputCulture)

    /// <summary> NormalizationRules </summary>
    /// <value> Represents the list of rules to normalize input text to unified form. </value>
    [<DefaultValue>]
    val mutable NormalizationRules : System.Collections.Generic.IDictionary<string, string>
        
    /// <summary> OriginCulture </summary>
    /// <value> Represents the source file culture (locale). This field is read-only. </value>
    member this.OriginCulture = 
        new CultureInfo(originCulture)
        
    /// <summary> OutputCulture </summary>
    /// <value> Represents the output text culture (locale). This field is read-only. </value>
    member this.OutputCulture = 
        new CultureInfo(outputCulture)