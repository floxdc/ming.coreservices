﻿module Floxdc.Ming.Core.Interfaces


type ILeafInfo = 
    abstract member Key: string
    abstract member Value: string