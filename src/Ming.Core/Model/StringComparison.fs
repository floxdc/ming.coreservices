﻿module Floxdc.Ming.Core.StringComparison

open Floxdc.Ming.Core.Interfaces


let private explode (s: string)= 
    [for c in s -> c]


let rec private compareChars cs' cs'' = 
    match cs', cs'' with
    | [], [] -> 0
    | _::_, [] -> 1
    | [], _::_ -> -1
    | x'::xs', x''::xs'' -> 
        let r = System.String.Compare(x'.ToString(), x''.ToString(), System.StringComparison.InvariantCulture)
        if r = 0 then compareChars xs' xs''
        else r
        

let compare currentValue leafInfo = 
    let cv = explode (currentValue :> ILeafInfo).Key
    let cli = explode (leafInfo :> ILeafInfo).Key
    compareChars cv cli

