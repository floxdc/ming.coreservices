﻿module Floxdc.Ming.Core.Tree

open Floxdc.Ming.Core.Interfaces
open Floxdc.Ming.Core.StringComparison


type 'a Tree = 
    | Nil
    | TreeNode of int * 'a Tree * 'a * 'a Tree


let internal height = function
    | TreeNode (h, _, _, _) -> h
    | Nil -> 0


let internal make l hd r = 
    let h = 1 + max (height l) (height r)
    TreeNode(h, l, hd, r)


let internal rotateLeft = function 
    | TreeNode(_, l, hd, TreeNode(_, rl, rhd, rr)) ->
        let l' = make l hd rl
        make l' rhd rr
    | node -> node 


let internal rotateRight = function
    | TreeNode(_, TreeNode(_, ll, lhd, lr), hd, r) -> 
        let r' = make lr hd r
        make ll lhd r'
    | node -> node


let internal doubleRotateLeft = function 
    | TreeNode(_, l, hd, r) -> 
        let r' = rotateRight r
        let node' = make l hd r'
        rotateLeft node'
    | node -> node


let internal doubleRotateRight = function 
    | TreeNode(_, l, hd, r) -> 
        let l' = rotateLeft l
        let node' = make l' hd r
        rotateRight node'
    | node -> node


let internal balanceFactor = function
    | Nil -> 0
    | TreeNode(_, l, _, r) ->
        (height l) - (height r)


let internal balance = function 
    // Left unbalanced
    | TreeNode(_, l, _, _) as node when
        balanceFactor node >= 2 ->
            //Left left case
            if balanceFactor l >= 1 then rotateRight node
            // Left right case
            else doubleRotateRight node
    // Right unbalanced
    | TreeNode(_, _, _, r) as node when 
        balanceFactor node <= -2 -> 
            // Right right case
            if balanceFactor r <= -1 then rotateLeft node
            // Right left case
            else doubleRotateLeft node
    // Right left case
    | node -> node


let rec internal contains v = function 
    | Nil -> false
    | TreeNode(_, l, hd, r) -> 
        let c = StringComparison.compare v hd
        if c = 0 then true
        else
            if c < 0 then contains v l
            else contains v r


let rec internal getValue k = function 
    | Nil -> System.String.Empty
    | TreeNode(_, l, hd, r) -> 
        let c = StringComparison.compare k hd
        if c = 0 then 
            let s = hd :> ILeafInfo
            s.Value
        else
            if c < 0 then getValue k l
            else getValue k r


let rec internal insert v = function 
    | Nil -> TreeNode(1, Nil, v, Nil)
    | TreeNode(_, l, hd, r) as node ->
        let c = StringComparison.compare v hd
        if c = 0 then node
        else 
            let l', r' = 
                if c < 0 then insert v l, r
                else l, insert v r
            let node' = make l' hd r'
            balance <| node'