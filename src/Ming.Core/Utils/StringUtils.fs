﻿module Floxdc.Ming.Core.StringUtils

open System.Globalization
open System.Text
open System.Text.RegularExpressions
    

let getFirstChar (value: string) = 
    match value.Length with 
    | 0 -> System.String.Empty
    | _ -> value.Substring(0, 1)


let isMatchToken token input = 
    Regex.IsMatch(input, ("^" + token))


let private liftRegister culture (target: string) = 
    target.ToUpper(culture)


let private normalize form (value: string) = 
    value.Trim().Normalize(form)
    |> String.filter (fun c -> System.Char.GetUnicodeCategory(c) <> UnicodeCategory.NonSpacingMark)


let normalizeAndCollapseSpaces inputCulture value = 
    let normalized = 
        liftRegister inputCulture value
        |> normalize NormalizationForm.FormC
    normalized.Replace(" ", System.String.Empty)


let normalizeInput inputCulture form value = 
    liftRegister inputCulture value
    |> normalize form


let trimToken (token: string) (value: string) = 
    value.Substring(token.Length)

