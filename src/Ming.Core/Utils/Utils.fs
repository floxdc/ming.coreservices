﻿module Floxdc.Ming.Core.Utils

open Floxdc.Ming.Core.BaseTypes
open Floxdc.Ming.Core.Interfaces

    
let rec getLeafKeys (acc: string list) (tree : AvlTree<ILeafInfo>) = 
    match tree.Height with 
    | 0 -> acc
    | _ -> 
        let leftKeys = getLeafKeys acc tree.Left
        let rightKeys = getLeafKeys acc tree.Right 
        acc @ [tree.Value.Key] @ leftKeys @ rightKeys


let sortMapValuesDesc acc (tokens: Map<_, _ list>) = 
    Map.fold(fun map key values -> 
        let sorted = List.sortDescending values
        map |> Map.remove key
        |> Map.add key sorted 
    ) acc tokens


let toSeq dictionary = 
    (dictionary :> seq<_>)
    |> Seq.map (|KeyValue|)


let rec private replaceSymbols (rules: (string * string) seq) (target: string) = 
    match (Seq.isEmpty rules) with 
    | true -> target
    | false -> 
        let oldValue, newValue = Seq.head rules
        target.Replace(oldValue, newValue)
        |> replaceSymbols (Seq.skip 1 rules)


let applyNormalizationRules rules target = 
    replaceSymbols rules target