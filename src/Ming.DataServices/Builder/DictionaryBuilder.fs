﻿namespace Floxdc.Ming.DataServices

open Floxdc.Ming.Core.Interfaces
open Floxdc.Ming.Core.BaseTypes
open Floxdc.Ming.Core.Tree


/// <summary> Initializes a new instance of the DictionaryBuilder class. </summary>
type DictionaryBuilder() = 
    let getNormalizedInput parseOptions source = 
        let getSources = DictionaryServices.SourceBuilder.getSourcesFromString parseOptions

        source 
        |> Seq.filter (System.String.IsNullOrWhiteSpace >> not)
        |> Seq.map getSources
        |> Seq.concat
        |> Seq.toList
    
    
    let insertAll (leafList: ILeafInfo list) = 
        List.fold (fun (tree: AvlTree<_>) e -> tree.Insert e) (new AvlTree<_>(Nil)) leafList


    /// <summary> Returns the tree representation of the dictionary source. </summary>
    /// <param name="parseOptions"> The instance of ParseOptions class. </param>
    /// <param name="source"> The source dictionary content. </param>
    /// <returns> AvlTree<ILeafInfo>. </returns>
    /// <exception cref="ArgumentException"> One of parseOptions members wasn't specified or empty. </exception>
    member this.BuildSourceTree parseOptions source = 
        match (Seq.isEmpty source) with 
        | true -> AvlTree(Nil)
        | false -> 
            getNormalizedInput parseOptions source
            |> DictionaryServices.LeafConverter.toLeafList
            |> insertAll


    /// <summary> Returns the new instance of DictionaryContainer class. </summary>
    /// <param name="opts"> The instance of ParseOptions class. </param>
    /// <param name="source"> The source dictionary content. </param>
    /// <param name="originCulture"> The culture (locale) of origin dictionary language specified by name. </param>
    /// <param name="innerCulture"> The culture of input text specified by name. </param>
    /// <param name="outerCulture"> The culture output text specified by name. </param>
    /// <returns> DictionaryContainer. </returns>
    /// <exception cref="ArgumentException"> One of parseOptions members wasn't specified or empty. </exception>
    member this.GetDictionaryContainer opts source originCulture innerCulture outerCulture = 
        let tree = this.BuildSourceTree opts source
        let container = new DictionaryContainer (tree, originCulture, innerCulture, outerCulture)
        container.NormalizationRules <- opts.NormalizationRules
        container


    /// <summary> Returns the collection of normalized rules, which uses to bring the input data to a unified form. </summary>
    /// <param name="ruleArray"> Array of normalization rules repreented by strings. </param>
    /// <returns> IDictionary<string, string>. </returns>
    /// <exception cref="ArgumentException"> Array of normalization rules contains odd number of elements. </exception>
    member this.GetNormalizationRules ruleArray = Infrastructure.getNormalizationRules ruleArray


    /// <summary> Returns the new instance of ParseOptions class. </summary>
    /// <param name="culture"> The culture of input text specified by name. </param>
    /// <returns> ParseOptions. </returns>
    /// <exception cref="ArgumentException"> <paramref name="culture"/> is empty. </exception>
    /// <exception cref="CultureNotFoundException"> <paramref name="culture"/> is not a valid culture name. </exception>
    member this.GetParseOptions culture = Infrastructure.getParseOptions culture