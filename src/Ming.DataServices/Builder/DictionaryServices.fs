﻿module Floxdc.Ming.DataServices.DictionaryServices


open Floxdc.Ming.Core
open Floxdc.Ming.Core.Interfaces
open Floxdc.Ming.Core.BaseTypes
open Floxdc.Ming.DataServices


module internal LeafConverter = 
    let private toLeafInfo (dictionaryItem: string * string) = 
        let (key, value) = dictionaryItem
        new LeafInfo(key, value) :> ILeafInfo


    let private toType func list = 
        list |> List.map func


    let toLeafList dictionary = 
        dictionary |> toType toLeafInfo


module internal SourceBuilder = 
    let private printArray (target: string array) = 
        "[|" + System.String.Join("; ", target) + "|]"


    let rec private getNodeValue variantIndex source = 
        if variantIndex < 0 then 
            let content = printArray source
            let ex = System.String.Format("Can't obtain 'Sub-value variant' value at desirable index or lesser in '{0}'. You should check correctness of source dictionary", content)
            raise (System.IndexOutOfRangeException ex)

        match (System.String.IsNullOrWhiteSpace source.[variantIndex]) with
        | true -> getNodeValue (variantIndex - 1) source
        | false -> source.[variantIndex]


    let private getNodeSourceValue (fullString: string) index source = 
        try 
            getNodeValue index source
        with 
        | :? System.IndexOutOfRangeException as iex -> 
            let ex = System.String.Format("Can't obtain value from string '{0}'. You should check correctness of source dictionary.", fullString)
            let e = new System.ArgumentException(ex, iex)
            raise e
        | ex -> 
            raise ex


    let private buildNodeSource (options: Infrastructure.ParseOptions) (source: string) = 
        let keyValuePair = source.Split(options.ValueSeparator)

        if (System.String.IsNullOrWhiteSpace keyValuePair.[0]) then
            let ex = System.String.Format("Can't obtain key from string '{0}'. You should check correctness of source dictionary.", source)
            raise (System.ArgumentException ex)
    
        if keyValuePair.Length <> 2 then
            let content = printArray keyValuePair
            let ex = System.String.Format("Can't separate key from values from the input string. Maybe 'ValueSeparator' option was set incorrectly. Separation result: '[|{0}|]'", content)
            raise (System.ArgumentException ex)

        let values = keyValuePair.[1].Split(options.SubvalueSeparator)

        let mutable variantIndex = options.SubvalueVariant
        if values.Length <= options.SubvalueVariant then
            variantIndex <- values.Length - 1
    
        let value = getNodeSourceValue source variantIndex values
        (keyValuePair.[0], value)


    let getSourcesFromString options source =
        let buildSource = buildNodeSource options
        let rulesSeq = Floxdc.Ming.Core.Utils.toSeq options.NormalizationRules
 
        let normalizedSource = 
            StringUtils.normalizeAndCollapseSpaces options.Culture source
            |>  Floxdc.Ming.Core.Utils.applyNormalizationRules rulesSeq

        normalizedSource.Split(options.PairSeparator)
        |> Array.map buildSource
        |> Array.toList