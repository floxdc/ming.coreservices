﻿module Floxdc.Ming.DataServices.Infrastructure

open System.Globalization
open System.Text


/// <summary> Initializes a new instance of the ParseOptions class to the value indicated by a source text culture (locale). </summary>
/// <param name="culture"> The name of the source text culture. </param>
/// <exception cref="CultureNotFoundException"> <paramref name="culture"/> is not a valid culture name. </exception>
type ParseOptions(culture: string) = 
        let mutable pairSeparator = [|'\t'|]
        let mutable subvalueSeparator = [|','|]
        let mutable subvalueVariant = 1
        let mutable valueSeparator = [|'—'|]
        let mutable normalizationRules = 
            new System.Collections.Generic.Dictionary<string, string>() :> System.Collections.Generic.IDictionary<string, string>
        
        
        /// <summary> Represents the name of the source file culture (locale). This field is read-only. </summary>
        member this.Culture = 
            new CultureInfo(culture)

        /// <summary> NormalizationRules </summary>
        /// <value> Gets or sets the collection of normalization rules. </value>
        member this.NormalizationRules
            with get() = normalizationRules
            and set(value) = normalizationRules <- value

        /// <summary> PairSeparator </summary>
        /// <value> Gets or sets the character to separate key-values pairs in the source file. </value>
        member this.PairSeparator
            with get() = pairSeparator
            and set(value) = pairSeparator <- value

        /// <summary> SubvalueSeparator </summary>
        /// <value> Gets or sets the character to separate different values in each pair in the source file. </value>
        member this.SubvalueSeparator
            with get() = subvalueSeparator
            and set(value) = subvalueSeparator <- value

        /// <summary> SubvalueVariant </summary>
        /// <value> Gets or sets the number of value to be extracted from the source file. </value>
        member this.SubvalueVariant
            with get() = subvalueVariant
            and set(value) = subvalueVariant <- value

        /// <summary> ValueSeparator </summary>
        /// <value> Gets or sets the character to separate values from keys in the source file. </value>
        member this.ValueSeparator
            with get() = valueSeparator
            and set(value) = valueSeparator <- value


let internal getParseOptions culture = 
    new ParseOptions(culture)


let internal getNormalizationRules (rules: string array) = 
    if (rules.Length % 2) <> 0 then
        raise (System.ArgumentException "Array of normalization rules must contain even number of elements.")

    let normalizationRules = new System.Collections.Generic.Dictionary<string, string>()

    for i in 1..2..rules.Length do 
        let key = rules.[i - 1].ToUpper(CultureInfo.InvariantCulture).Normalize(NormalizationForm.FormC)
        let value = rules.[i].ToUpper(CultureInfo.InvariantCulture).Normalize(NormalizationForm.FormC)
        normalizationRules.Add(key, value)
    
    normalizationRules :> System.Collections.Generic.IDictionary<string, string>