﻿module Floxdc.Ming.Tests.Utils 

open System.IO
open Nessos.FsPickler.Json
open Floxdc.Ming.Core.BaseTypes


let private readDictionaryContainer path = 
    let serializer = FsPickler.CreateJsonSerializer()
    use stream = new System.IO.FileStream(path, FileMode.Open, FileAccess.Read)
    serializer.Deserialize<DictionaryContainer>(stream)

    
let getContainer filePath =
    readDictionaryContainer filePath