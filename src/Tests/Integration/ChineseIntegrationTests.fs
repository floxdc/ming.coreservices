﻿namespace Floxdc.Ming.Tests.Integration

open Xunit
open Floxdc.Ming.Tests

module Chinese = 

    
    let dicPath = @"c:\Projects\Ming\Dictionaries\zh-Hans_en_ru+Pinyin.dic"

    
    [<Theory>]
    [<InlineData("Beijing", "Бэйцзин")>]
    [<InlineData("bin", "Бинь")>]
    [<InlineData("ho", "Ho")>]
    [<InlineData("huo", "Хо")>]
    [<InlineData("Li Hua", "Ли Хуа")>]
    [<InlineData("lüe", "Люэ")>]
    [<InlineData("Mao Zedong", "Мао Цзэдун")>]
    let ``Should transcribes words from english to russian with C-form normalization`` (input, output) = 
        let container = Utils.getContainer dicPath
        let ming = new Floxdc.Ming.Control.Ming(container)

        let result = ming.Transcribe input

        Assert.Equal(output, result)

    
    [<Theory>]
    [<InlineData("Beǐjīng", "Бэйцзин")>]
    [<InlineData("Lǐ Huá", "Ли Хуа")>]
    let ``Should transcribes words from english to russian with D-form normalization`` (input, output) = 
        let container = Utils.getContainer dicPath
        let ming = new Floxdc.Ming.Control.Ming(container)

        let result = ming.Transcribe(input, "d") 

        Assert.Equal(output, result)

    
    [<Theory>]
    [<InlineData("Beijing", "Бэйцзин")>]
    [<InlineData("Beǐjīng", "Бэйцзин")>]
    [<InlineData("bin", "Бинь")>]
    [<InlineData("huo", "Хо")>]
    [<InlineData("Li Hua", "Ли Хуа")>]
    [<InlineData("Lǐ Huá", "Ли Хуа")>]
    [<InlineData("lüe", "Люэ")>]
    [<InlineData("Mao Zedong", "Мао Цзэдун")>]
    let ``Should transcribes words from english to russian in auto mode`` (input, output) = 
        let container = Utils.getContainer dicPath
        let ming = new Floxdc.Ming.Control.Ming(container)

        let mutable result = System.String.Empty

        let isSuccessful = ming.TryAuto(input, &result)

        Assert.True(isSuccessful)
        Assert.Equal(output, result)

    
    [<Theory>]
    [<InlineData("ho", "Ho")>]
    [<InlineData("Sun Yat-sen", "Сунь Яt-Сэнь")>]
    let ``Should not transcribes words hasn't fit the dictionary`` (input, output) = 
        let container = Utils.getContainer dicPath
        let ming = new Floxdc.Ming.Control.Ming(container)

        let mutable result = System.String.Empty

        let isSuccessful = ming.TryAuto(input, &result)

        Assert.False(isSuccessful)
        Assert.Equal(output, result)


//    open System.IO
//    open Xunit
//
//
//    let dicPath = @"c:\Projects\Ming\Dictionaries\zh-Hans_en-US_ru-RU.dic"
//
//
//    let executeTest args = 
//        let stream = new MemoryStream(1000)
//        let writer = new StreamWriter(stream)
//        System.Console.SetOut(writer)
//        
//        let assembly = System.Reflection.Assembly.LoadFrom(@"c:\Projects\Ming\Ming.Console\bin\Debug\Ming.Console.exe")
//        let point = assembly.EntryPoint
//        let go = assembly.CreateInstance(point.Name)
//
//        assembly.EntryPoint.Invoke(go, args) |> ignore
//
//        let result = System.Text.Encoding.Default.GetString(stream.ToArray())
//        stream.Close()
//        result
//
//
//    let getEntryPointArgd args : obj[] = 
//        [|args|]
//
//
//    [<Theory>]
//    [<InlineData("bin", "Бинь")>]
//    let ``x`` (input, output) = 
//        let args = getEntryPointArgd [|"/t"; input; "/p"; dicPath|]
//        
//        let result = executeTest args
//        
//        Assert.Equal(output, result)