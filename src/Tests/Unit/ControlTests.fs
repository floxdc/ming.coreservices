﻿namespace Floxdc.Ming.Tests.Control


module MingTests = 

    open Xunit
    open Floxdc.Ming.Control

    let ming = 
        let builder = new Floxdc.Ming.DataServices.DictionaryBuilder()
        let source = Seq.ofList ["a — а"; "b — б"; "c — к"; "d — д"; "e — е"; "f — ф"; "g — ж"]
        let opts = builder.GetParseOptions "en-US"
        opts.NormalizationRules <- builder.GetNormalizationRules [|"a"; "b"|]
        let container = builder.GetDictionaryContainer opts source "zh-Hans" "en-US" "ru-RU"
        new Ming(container)

    let ming' = 
        let builder = new Floxdc.Ming.DataServices.DictionaryBuilder()
        let source = Seq.ofList ["a — а"; "b — б"; "c — к"; "d — д"; "e — е"; "f — ф"; "g — ж"]
        let opts = builder.GetParseOptions "en-US"
        opts.NormalizationRules <- builder.GetNormalizationRules [|"a"; "b"|]
        let container = builder.GetDictionaryContainer opts source "zh-Hans" "en-US" "ru-RU"
        new Ming(container, true)


    [<Fact>]
    let ``Transcribe should transcribes simple string``() = 
        let s = "abc"

        let result = ming.Transcribe s

        Assert.Equal("Аак", result)


    [<Fact>]
    let ``Transcribe should transcribes empty string``() = 
        let s = "  "

        let result = ming.Transcribe s

        Assert.Equal(System.String.Empty, result)


    [<Fact>]
    let ``Transcribe should transcribes string with spaces and hyphens``() = 
        let s = "abc abc-de"

        let result = ming.Transcribe s

        Assert.Equal("Аак Аак-Де", result)


    [<Theory>]
    [<InlineData("abc Hij-de", "Аак Hij-Де")>]
    [<InlineData("abchij hij-de", "Аакhij Hij-Де")>]
    [<InlineData("abc hij-hij-de", "Аак Hij-Hij-Де")>]
    [<InlineData("abc hij", "Аак Hij")>]
    [<InlineData("abc hijde", "Аак Hijде")>]
    [<InlineData("hij", "Hij")>]
    let ``Transcribe should transcribes unknown strings``(input, output) = 
        let result = ming.Transcribe input

        Assert.Equal(output, result)


    [<Theory>]
    [<InlineData("abc hij-de", "Аак [Hij]-Де")>]
    [<InlineData("abchij hij-de", "Аак[Hij Hij]-Де")>]
    [<InlineData("abc hij-hij-de", "Аак [Hij-Hij]-Де")>]
    [<InlineData("abc hij", "Аак [Hij]")>]
    [<InlineData("abc hijde", "Аак [Hij]Де")>]
    [<InlineData("hij", "[Hij]")>]
    let ``Transcribe should transcribes unknown strings with untranscribed marks``(input, output) = 
        let result = ming'.Transcribe input
        
        Assert.Equal(output, result)