﻿namespace Floxdc.Ming.Tests.Core

// TODO: add tree tests
open Xunit
open Floxdc.Ming.Core


module StringComparisonTests = 
    
    open Floxdc.Ming.Core.BaseTypes
    open Floxdc.Ming.Core.StringComparison


    [<Fact>]
    let ``Two equal leaf keys should returns zero then compares``() = 
        let s1 = new LeafInfo("abc", "")
        let s2 = new LeafInfo("abc", "")

        let result = compare s1 s2
        
        Assert.Equal(0, result)


    [<Fact>]
    let ``Two empty leaf keys should returns zero then compares``() = 
        let s1 = new LeafInfo("", "")
        let s2 = new LeafInfo("", "")

        let result = compare s1 s2
        
        Assert.Equal(0, result)


    [<Fact>]
    let ``Lesser leaf key of two should returns minus one then compares with greater``() = 
        let s1 = new LeafInfo("abb", "")
        let s2 = new LeafInfo("abc", "")

        let result = compare s1 s2
        
        Assert.Equal(-1, result)


    [<Fact>]
    let ``Greater leaf key of two should returns plus one then compares with lesser``() = 
        let s1 = new LeafInfo("abc", "")
        let s2 = new LeafInfo("abb", "")

        let result = compare s1 s2
        
        Assert.Equal(1, result)


module StringUtilsTest = 

    open Floxdc.Ming.Core.StringUtils


    [<Fact>]
    let ``getFirstChar should returns first character of non-empty string``() = 
        let s = "test"

        let result = getFirstChar s

        Assert.Equal("t", result)


    [<Fact>]
    let ``getFirstChar should returns none when empty string passes``() = 
        let s1 = System.String.Empty
        let s2 = ""

        let result1 = getFirstChar s1
        let result2 = getFirstChar s2

        Assert.Equal(System.String.Empty, result1)
        Assert.Equal(System.String.Empty, result2)


    [<Fact>]
    let ``normalizeInput should returns normalized string``() = 
        let s1 = "abc"
        let s2 = "ABC"
        let s3 = ""

        let result1 = normalizeInput System.Globalization.CultureInfo.CurrentCulture System.Text.NormalizationForm.FormC s1
        let result3 = normalizeInput System.Globalization.CultureInfo.CurrentCulture System.Text.NormalizationForm.FormC s2
        let result4 = normalizeInput System.Globalization.CultureInfo.CurrentCulture System.Text.NormalizationForm.FormC s3

        Assert.Equal("ABC", result1)
        Assert.Equal("ABC", result3)
        Assert.Equal("", result4)


    [<Fact>]
    let ``normalizeAndCollapseSpaces should returns normalized and collapse internal spaces in string``() = 
        let s = "a b  c"

        let result = normalizeAndCollapseSpaces System.Globalization.CultureInfo.CurrentCulture s

        Assert.Equal("ABC", result)


module UtilsTest = 
    
    open Floxdc.Ming.Core.Utils


    [<Fact>]
    let ``applyNormalizationRules should replaces complex symbols to simplify text input``() = 
        let rules = [("a", "b"); ("c", "c")] |> Seq.ofList
        let s = "abc"

        let result = applyNormalizationRules rules s

        Assert.Equal("bbc", result)


    [<Fact>]
    let ``applyNormalizationRules shouldn't replaces complex symbols when no rules are specified``() = 
        let rules = Seq.empty
        let s = "abc"

        let result = applyNormalizationRules rules s

        Assert.Equal(s, result)