﻿namespace Floxdc.Ming.Tests.DataServices

open Xunit


module DictionaryBuilderTests = 
    
    open Floxdc.Ming.DataServices


    let builder = new DictionaryBuilder()


    [<Fact>]
    let ``BuildSourceTree should builds ILeafInfo tree from source string``() = 
        let ss = Seq.ofList ["a — b \tc— d"; ""; "e — f, i \tg—h"]
        let opts = builder.GetParseOptions "en-US"
        opts.NormalizationRules <- builder.GetNormalizationRules [|"a"; "b"|]

        let result = builder.BuildSourceTree opts ss

        Assert.IsType(typeof<Floxdc.Ming.Core.BaseTypes.AvlTree<Floxdc.Ming.Core.Interfaces.ILeafInfo>>, result)
        Assert.Equal(3, result.Height)
        Assert.Equal("C", result.Value.Key)


    [<Fact>]
    let ``BuildSourceTree should builds empty tree from empty source string``() = 
        let ss = Seq.ofList []
        let opts = builder.GetParseOptions "en-US"
        opts.NormalizationRules <- builder.GetNormalizationRules [|"a"; "b"|]

        let result = builder.BuildSourceTree opts ss

        Assert.IsType(typeof<Floxdc.Ming.Core.BaseTypes.AvlTree<Floxdc.Ming.Core.Interfaces.ILeafInfo>>, result)
        Assert.Equal(0, result.Height)


    [<Fact>]
    let ``BuildSourceTree should throws exception when can't obtain key from source dictionary``() = 
        let ss = Seq.ofList ["— b \tc— d"]
        let opts = builder.GetParseOptions "en-US"
        opts.NormalizationRules <- builder.GetNormalizationRules [|"a"; "b"|]

        Assert.Throws<System.ArgumentException>(fun () -> builder.BuildSourceTree opts ss |> ignore)


    [<Fact>]
    let ``BuildSourceTree should throws exception when can't obtain value from source dictionary``() = 
        let ss = Seq.ofList ["a —   \tc— d"]
        let opts = builder.GetParseOptions "en-US"
        opts.NormalizationRules <- builder.GetNormalizationRules [|"a"; "b"|]

        Assert.Throws<System.ArgumentException>(fun () -> builder.BuildSourceTree opts ss |> ignore)


    [<Fact>]
    let ``GetNormalizationRules should converts array of strings to normalized key-value collection``() = 
        let a = [|"a"; "b"; "Ü"; "V"|]

        let result = builder.GetNormalizationRules a
        let v = result.Item("A")

        Assert.IsType(typeof<System.Collections.Generic.Dictionary<string, string>>, result)
        Assert.Equal(2, result.Count)
        Assert.Equal("B", v)


    [<Fact>]
    let ``GetNormalizationRules should converts empty array of strings to empty collection``() = 
        let a = [||]

        let result = builder.GetNormalizationRules a

        Assert.IsType(typeof<System.Collections.Generic.Dictionary<string, string>>, result)
        Assert.Equal(0, result.Count)


    [<Fact>]
    let ``GetNormalizationRules should throws exception when input array is contains odd number of elements``() = 
        let a = [|"a"|]

        Assert.Throws<System.ArgumentException>(fun () -> builder.GetNormalizationRules a |> ignore)


    [<Fact>]
    let ``GetParseOptions should returns mutable ParseOptions object with preset parameters``() = 
        let c = "en-US"
        let rs = builder.GetNormalizationRules [||]

        let result = builder.GetParseOptions c

        Assert.IsType(typeof<Floxdc.Ming.DataServices.Infrastructure.ParseOptions>, result)
        Assert.Equal<System.Collections.Generic.IDictionary<string, string>>(rs, result.NormalizationRules)
        Assert.Equal('\t', result.PairSeparator.[0])
        Assert.Equal(',', result.SubvalueSeparator.[0])
        Assert.Equal(1, result.SubvalueVariant)
        Assert.Equal('—', result.ValueSeparator.[0])


    [<Fact>]
    let ``GetParseOptions should returns mutable ParseOptions``() = 
        let c = "en-US"
        let rs = builder.GetNormalizationRules [||]
        let ch = '1'
        let chs = [|ch|]

        let result = builder.GetParseOptions c
        result.PairSeparator <- chs
        result.SubvalueSeparator <- chs
        result.SubvalueVariant <- 2
        result.ValueSeparator <- chs

        Assert.IsType(typeof<Floxdc.Ming.DataServices.Infrastructure.ParseOptions>, result)
        Assert.Equal(ch, result.PairSeparator.[0])
        Assert.Equal(ch, result.SubvalueSeparator.[0])
        Assert.Equal(2, result.SubvalueVariant)
        Assert.Equal(ch, result.ValueSeparator.[0])