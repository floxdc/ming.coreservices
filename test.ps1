$root = (Get-Location).Path

cmd.exe /c 'C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\VsDevCmd.bat'

$path = $root + '\src\Tests'
cd $path
dotnet restore
dotnet build
dotnet test

Write-Host "Done."